# .dot to .vtu converter
This tool turns a .dot graph into a 3D representation (.vtu) for viewing in e.g. Paraview.

## Example graph
This is an example of a very simple graph (two node connected by a single edge). Using the added coordinates for the nodes, the dot2vtu converter is able to create a 3D representation of this graph.

<code>graph G{<br>
&nbsp; 0 [spatial_node="0 0 0"];<br>
&nbsp; 1 [spatial_node="1 1 1"];<br>
&nbsp; 0--1;<br>
}</code>

Note: Node names must be integers.
Coordinates must be added with the spatial_node flag for each node.
Coordinates must be integers. 

## Requirements
Required packages are listed in requirements.txt and can be installed using pip as follows:\
`pip3 install -r requirements.txt`

## Input
- .dot graph containing nodes, edges and coordinate labels.
- The name of the output file to write can be supplied with the -o flag. Otherwise, the output will be written to the file result.vtu.

## Output
- .vtu file

## Usage
`python3 dot2vtu.py graph.dot`

