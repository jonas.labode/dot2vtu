import argparse
import networkx as nx
from vtkmodules.vtkCommonCore import vtkPoints
from vtkmodules.vtkCommonDataModel import (
    VTK_LINE,
    VTK_VERTEX,
    vtkUnstructuredGrid
)
from vtkmodules.vtkIOXML import vtkXMLUnstructuredGridWriter

# see https://kitware.github.io/vtk-examples/site/Python/UnstructuredGrid/UGrid/
# https://www.programcreek.com/python/example/112789/vtk.vtkXMLUnstructuredGridWriter


def read_graph(file):
    # Read graph from .dot file and turn it into a networkx graph
    # https://networkx.org/documentation/stable/reference/generated/networkx.drawing.nx_pydot.read_dot.html
    dot_graph = nx.Graph(nx.drawing.nx_pydot.read_dot(file))

    return dot_graph


def create_points(nx_graph):
    nodes = list(nx_graph.nodes)
    points = vtkPoints()

    for node in nodes:
        # TODO: When reading some .dot files a \n is included in the node list.
        #  This try catch block is here to handle this case, but there should be a better option
        try:
            coordinates = nx_graph.nodes[node]['spatial_node']
            coordinates = coordinates.replace('"', '')
            coordinates = coordinates.split(' ')
            # TODO: This only works, if the name of the node is indeed an int
            points.InsertPoint(int(node), [int(coordinates[0]), int(coordinates[1]), int(coordinates[2])])
        except KeyError:
            continue

    return points


def create_grid(points, nx_graph):
    nodes = list(nx_graph.nodes)
    edges = list(nx_graph.edges)
    ugrid = vtkUnstructuredGrid()
    ugrid.SetPoints(points)
    # TODO: This method might be included to allocate initial storage for the cell connectivity. Default = 1000
    # ugrid.Allocate(xxx)

    for node in nodes:
        # TODO: When reading some .dot files a \n is included in the node list.
        #  This try catch block is here to handle this case, but there should be a better option
        try:
            ugrid.InsertNextCell(VTK_VERTEX, 1, [int(node)])
        except ValueError:
            continue

    for edge in edges:
        ugrid.InsertNextCell(VTK_LINE, 2, [int(edge[0]), int(edge[1])])

    return ugrid


def write_file(ugrid, filename='result.vtu'):
    writer = vtkXMLUnstructuredGridWriter()
    writer.SetFileName(filename)
    writer.SetInputData(ugrid)
    writer.Write()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Converts a .dot graph to a .vtu file with spatial context')
    parser.add_argument('input_file', action='store', type=str, help='.dot file to analyze')
    parser.add_argument('-o', '--output_file', action='store', type=str, required=False, help='Name of output file')

    args = parser.parse_args()

    input_file = args.input_file
    output_file = args.output_file

    graph = read_graph(input_file)
    pts = create_points(graph)
    grid = create_grid(pts, graph)
    if output_file:
        write_file(grid, output_file)
    else:
        write_file(grid)
